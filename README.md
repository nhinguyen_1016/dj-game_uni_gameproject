# Listen To The Beat - Team LTTB

**Wanna be a DJ?**


## Project Members

- Nico Trölitzsch 894740, 
- Andreas Katkov, 908539,
- Tim Kaiser 904966, 
- Nhi Nguyen 909636, 
- Alina Schmidt 875800, 


## Project description

Der Spieler übernimmt die Rolle eines DJs und versucht das Publikum zu begeistern.
Dafür passt er/sie die Musik mithilfe eines Mischpultes den Wünschen des Publikums an.
Das Publikum belohnt diesen mit Punkten. Für Fehler des Spielers gibt es Minuspunkte.
Das Ziel ist es jedes Level mit einem möglichst hohem Score abzuschließen. 


## Tooling

- Git
- Confluence
- Unity
- Photoshop
- Clip Studio Paint
- Audacity
- Blender



## Tech

language C#
