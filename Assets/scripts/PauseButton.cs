using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButton : MonoBehaviour
{
    private Camera mainCamera;
    private Ray ray;
    private RaycastHit hit;
    [SerializeField]
    private LayerMask mask;
    private new Renderer renderer;
    [SerializeField]
    private GameObject pause, play;
    private MixConsole mixConsole;

    public IngameAudioGod IngameAudioGod;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        renderer = GetComponent<Renderer>();
        mixConsole = MixConsole.getInstance();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f, mask))
            {
                if (transform == hit.transform)
                {               
                    IngameAudioGod.playBClickOff();     
                    play.SetActive(true);
                    pause.SetActive(false);
                    if (this.name.Equals("PauseLeft"))
                    {
                        mixConsole.pauseLeft();
                    }
                    else
                    {
                        mixConsole.pauseRight();
                    }
                }
            }


        }
    }
    //equal to raycast in update
    // private void OnMouseDown(){
    //     Debug.Log(renderer.material.mainTexture.name);
    //     renderer.material.mainTexture  = 
    //         renderer.material.mainTexture  == play ? pause : play;
    // }
}
