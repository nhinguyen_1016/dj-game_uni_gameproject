using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameDeckDisplay : MonoBehaviour
{
    public Transform ingameDeckTransform;
    public IngameDeckEntityDisplay deckEDPrefab;

    public Transform screenDisplaysT;
    public IngameDeckEntityDisplay sDisplayPrefabL;
    public IngameDeckEntityDisplay sDisplayPrefabR;

    // Start is called before the first frame update
    void Start()
    {
        // Debug.Log("Hi von IngameDeckDisplay");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Prime(List<Sample> samples){
        for(int i = 0; i < ingameDeckTransform.childCount; i++){
            GameObject child = ingameDeckTransform.GetChild(i).gameObject;
            Destroy(child);
        }
        foreach(Sample s in samples)
        {
            // ist s in eines der Displays?
            if(!MixConsole.getInstance().isOnDisplay(s)){
                IngameDeckEntityDisplay IDED = (IngameDeckEntityDisplay)Instantiate(deckEDPrefab);
                IDED.transform.SetParent(ingameDeckTransform, false);
                IDED.Prime(s);
            }
        }
    }

    IngameDeckEntityDisplay SCL;
    IngameDeckEntityDisplay SCR;

    public void PrimeScreens(Sample sL, Sample sR){
        for(int i = 0; i < screenDisplaysT.childCount; i++)
        {
            GameObject child = screenDisplaysT.GetChild(i).gameObject;
            Destroy(child);
        }

        SCL = (IngameDeckEntityDisplay)Instantiate(sDisplayPrefabL);
        SCL.transform.SetParent(screenDisplaysT, false);
        SCL.Prime(sL);

        SCR = (IngameDeckEntityDisplay)Instantiate(sDisplayPrefabR);
        SCR.transform.SetParent(screenDisplaysT, false);
        SCR.Prime(sR);

    }

    public void setTimes(bool isL, float secs)
    {
        if(isL){
            SCL.setTime(secs);
        }
        else{
            SCR.setTime(secs);
        }
    }
}
