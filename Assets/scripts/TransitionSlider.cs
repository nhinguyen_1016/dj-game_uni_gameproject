using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionSlider : MonoBehaviour
{
    private Vector3 startPosition;
    private Vector3 endPosition;
    public float volumeLeft;
    public float volumeRight;

    private MixConsole mixConsole;
    private Camera mainCamera;
    private bool start;
    private float currentDistanz;
    private RaycastHit hit;
    [SerializeField]
    private LayerMask mask;
    private Ray currentRay;
    [SerializeField]
    GameObject transitionSlider;

    float doubleClickTime = .2f, lastClickTime;

    public IngameAudioGod IngameAudioGod;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        startPosition = transitionSlider.transform.position;
        endPosition = startPosition + new Vector3(1.0f, 0.0f, 0f);
        volumeLeft = 1f;
        volumeRight = 0f;
        mixConsole = MixConsole.getInstance();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            
            currentRay = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(currentRay, out hit, 50.0f, mask))
            {
                if (transform == hit.transform)
                {
                    IngameAudioGod.playSlideMove();
                    float timeSinceLastClick = Time.time - lastClickTime;

                    if (timeSinceLastClick <= doubleClickTime){
                        start = false;
                        transitionSlider.transform.position = Vector3.Lerp(startPosition, endPosition, 0);
                        volumeLeft = 1f;
                        volumeRight = 0f;
                        Debug.Log("Double");
                    }else{
                        start = true;
                        calcVolume();
                        currentDistanz = 0;
                    }

                    lastClickTime = Time.time;
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (start)
            {
                start = false;
                currentDistanz = 0;
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (start)
            {
                calcVolume();
            }
        }
        mixConsole.mixSamples(volumeLeft, volumeRight);
    }

    private void calcVolume()
    {
        currentRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Vector3 currentPoint = currentRay.GetPoint(hit.distance);
        float distanz = (currentPoint.x - startPosition.x) / Mathf.Abs(startPosition.x -  endPosition.x);
        currentDistanz = Mathf.Max(0, Mathf.Min(distanz, 1));
        transitionSlider.transform.position = Vector3.Lerp(startPosition, endPosition, currentDistanz);
        volumeLeft = 1 - currentDistanz < 0 ? 0 : 1 - currentDistanz > 1 ? 1 : 1 - currentDistanz;
        volumeRight = currentDistanz < 0 ? 0 : currentDistanz > 1 ? 1 : currentDistanz;

    }    
}
