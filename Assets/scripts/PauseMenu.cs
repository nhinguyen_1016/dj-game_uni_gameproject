using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{

    public static bool isPaused = false;
    public GameObject pauseMenuUI;
    private static MixConsole mixConsole;
    private static Audience audience;

    // Start is called before the first frame update
    void Start()
    {
        mixConsole = MixConsole.getInstance();
        audience = Audience.getInstance();
    }

    // Update is called once per frame
    void Update(){
        if(Input.GetKeyDown(KeyCode.Escape)){
            if(isPaused){
                Resume();

            }else{
                Pause();
            }
        } 
    }

    public void Resume(){
        audience.resumeSpeech();
        audience.setPlaying(true);
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        mixConsole.playLeft();
        mixConsole.playRight();
        
    }

    public void Pause(){
        audience.pauseSpeech();
        audience.setPlaying(false);
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
        mixConsole.pauseLeft();
        mixConsole.pauseRight();

    }

    public void Restart(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadMainMenu(){
        Debug.Log("Loading main menu..");
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame(){
        Debug.Log("Quitting game.");
        Application.Quit();
   
    }
}
