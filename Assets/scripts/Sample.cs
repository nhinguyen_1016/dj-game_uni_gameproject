using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Sample", menuName = "Create New Sample", order = 0)]
public class Sample : ScriptableObject {
    // Sounddatei aus "sounds"
    public AudioClip aClip;
    
    // Werte
    public string composer, title;
    
    //
    [Range(0,10)]
    public int rock, funk, jazz, techno, pop, hiphop;

    //fürs erste public, da wir noch keine Komponente haben, welche den Sample freischalten kann
    public bool unlocked;
    // public int[] goodTransSecs;

    public bool played = false;


    // Methoden
    public void lockUnlock()
    {
        unlocked = !unlocked;
    }

}
