using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeSlider : MonoBehaviour
{
    private Vector3 startPosition;
    private Vector3 endPosition;
    public float volume;

    private MixConsole mixConsole;
    private Camera mainCamera;
    private bool start;
    private float currentDistanz;
    private RaycastHit hit;
    [SerializeField]
    private LayerMask mask;
    private Ray currentRay;
    [SerializeField]
    GameObject volumeSlider;

    float doubleClickTime = .2f, lastClickTime;

    public IngameAudioGod IngameAudioGod;

    // Start is called before the first frame update
    void Start()
    {
        mixConsole = MixConsole.getInstance();
        mainCamera = Camera.main;
        startPosition = volumeSlider.transform.position;
        endPosition = startPosition + new Vector3(0.0f, 0.45f, 0.45f);
        volume = 0;
        volumeSlider.transform.position = Vector3.Lerp(startPosition, endPosition, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            
            if (start)
            {
                calcVolume();
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            currentRay = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(currentRay, out hit, 50.0f, mask))
            {
                if (transform == hit.transform)
                {                  
                    IngameAudioGod.playSlideMove();
                    float timeSinceLastClick = Time.time - lastClickTime;

                    if (timeSinceLastClick <= doubleClickTime){
                        start = false;
                        volumeSlider.transform.position = Vector3.Lerp(startPosition, endPosition, 0);
                        volume = 0f;
                        Debug.Log("Double");
                    }else{
                        start = true;
                        calcVolume();
                        currentDistanz = 0;
                    }

                    lastClickTime = Time.time;
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (start)
            {
                start = false;
                currentDistanz = 0;
            }
        }
        
        UpdateVolume(volume);
    }

    private void calcVolume()
    {
        currentRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Vector3 currentPoint = currentRay.GetPoint(hit.distance);
        float distanz = (currentPoint.y - startPosition.y) / Mathf.Abs(startPosition.y -  endPosition.y);
        currentDistanz = Mathf.Max(0, Mathf.Min(distanz, 1));
        currentDistanz = currentDistanz switch
        {
            float f when f < 0.05f => 0,
            float f when f >= 0.05f && f < 0.15f => 0.1f,
            float f when f >= 0.15f && f < 0.25f => 0.2f,
            float f when f >= 0.25f && f < 0.35f => 0.3f,
            float f when f >= 0.35f && f < 0.45f => 0.4f,
            float f when f >= 0.45f && f < 0.55f => 0.5f,
            float f when f >= 0.55f && f < 0.65f => 0.6f,
            float f when f >= 0.65f && f < 0.75f => 0.7f,
            float f when f >= 0.75f && f < 0.85f => 0.8f,
            float f when f >= 0.85f && f < 0.95f => 0.9f,
            float f when f >=  0.95f => 1f,
            _ => 0f
        };
        volumeSlider.transform.position = Vector3.Lerp(startPosition, endPosition, currentDistanz);
        volume = currentDistanz * 2f;
    }    

    private void UpdateVolume(float volume)
    {
        if (this.name.Equals("VolumeRangeRight"))
        {
            mixConsole.changeVolume(volume, false);
        }
        else
        {
            mixConsole.changeVolume(volume, true);
        }
    }
}
