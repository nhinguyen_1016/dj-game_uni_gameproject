using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class speedKnop : MonoBehaviour
{
    private Quaternion minPosition;
    private Quaternion maxPosition;
    public float speed;
    private MixConsole mixConsole;
    [Range(0, 1)]
    private Camera mainCamera;
    private Vector2 changeStart;
    private Ray currentRay;
    private RaycastHit hit;
    [SerializeField]
    private LayerMask mask;
    private bool start;
    [Range(0, 1)]
    private float currentAngle;

    float doubleClickTime = .2f, lastClickTime;

    public IngameAudioGod IngameAudioGod;

    // Start is called before the first frame update
    void Start()
    {
        mixConsole = MixConsole.getInstance();
        mainCamera = Camera.main;
        speed = 1f;
        minPosition = transform.localRotation * Quaternion.Euler(0f, 0f, -90f);//Quaternion.Euler(-90f, 0f, 0f) * Quaternion.Euler(0f, 0f, -90f);
        maxPosition = transform.localRotation * Quaternion.Euler(0f, 0f, 90f);//minPosition * Quaternion.Euler(0f, 0f, -180f);
        transform.localRotation = Quaternion.Slerp(minPosition, maxPosition, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            
            currentRay = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(currentRay, out hit, 50.0f, mask))
            {
                if (transform == hit.transform)
                {
                    IngameAudioGod.playSlideMove();
                    float timeSinceLastClick = Time.time - lastClickTime;

                    if (timeSinceLastClick <= doubleClickTime){
                        start = false;
                        transform.localRotation = Quaternion.Lerp(minPosition, maxPosition, 0.5f);
                        speed = 1f;
                        Debug.Log("Double");
                    }else{
                        calcSpeed();
                        start = true;
                    }

                    lastClickTime = Time.time;  
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (start)
            {
                start = false;
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (start)
            {
                calcSpeed();
            }
        }
        if (this.name.Equals("SpeedKnobLeft"))
        {
            mixConsole.changeSpeed(speed, true);
        }
        else
        {
            mixConsole.changeSpeed(speed, false);
        }
    }

    private void calcSpeed()
    {
        currentRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Vector3 currentPoint = currentRay.GetPoint(hit.distance);
        /* Debug.Log(currentPoint);
        Debug.Log(transform.position);
        Debug.Log(currentPoint - transform.position);
        Debug.Log(Vector3.left);
        Debug.Log(transform.position);
        Debug.Log(Vector3.left - transform.position); */
        float angle = Vector2.SignedAngle(currentPoint - transform.position, Vector3.left);
        Debug.Log(angle);
        currentAngle = angle switch
        {
            float f when f >= 0f && f < 9f => 0,
            float f when f >= 9f && f < 27f => 0.1f,
            float f when f >= 27f && f < 45f => 0.2f,
            float f when f >= 45f && f < 63f => 0.3f,
            float f when f >= 63f && f < 81f => 0.4f,
            float f when f >= 81f && f < 99f => 0.5f,
            float f when f >= 99f && f < 117f => 0.6f,
            float f when f >= 117f && f < 135f => 0.7f,
            float f when f >= 135f && f < 153f => 0.8f,
            float f when f >= 153f && f < 171f => 0.9f,
            float f when f >=  171f && f <= 180f => 1f,
            _ => currentAngle
        };
        Debug.Log(currentAngle);
        transform.localRotation = Quaternion.Slerp(minPosition, maxPosition, currentAngle);
        speed = currentAngle * 2f;
        Debug.Log(speed);
    }

    public float getSpeed()
    {
        return speed;
    }
}
