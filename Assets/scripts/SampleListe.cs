using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SampleListe : MonoBehaviour
{
    private List<Sample> sampleListe  = new List<Sample>();
    public SampleListDisplay sampleListDisplayPrefab;
    public Deck deck;

    SampleListDisplay sampleListD;
    public static bool neueAenderung = false;

    // Start is called before the first frame update
    void Start()
    {
        // Debug.Log("Heyo!");
        initSamples();
        sampleListD = (SampleListDisplay)Instantiate(sampleListDisplayPrefab);
        sampleListD.Prime(sampleListe);
        sampleListD.PrimeDeck(deck.entities);
        sampleListD.PrimeText(deck.entities.Count, deck.getMaxSize());
    }

    // Update is called once per frame
    void Update()
    {
        // etwas änderte sich? prime alles
        if(neueAenderung){
            //Debug.Log("Veraenderung entdeckt!");
            initSamples();
            sampleListD.Prime(sampleListe);
            sampleListD.PrimeDeck(deck.entities);
            sampleListD.PrimeText(deck.entities.Count, deck.getMaxSize());
        }
        neueAenderung = false;
    }

    void initSamples(){
	    sampleListe.Clear();
        foreach (Sample foundSample in Resources.LoadAll<Sample>(""))
        {	    
            // ist sample freigeschaltet und ist es nicht im Deck vorhanden?
		    if(foundSample.unlocked && (deck.entities.IndexOf(foundSample) < 0)){
                //Debug.Log("Added sample to sample list.");
                sampleListe.Add(foundSample);
		    }
	    }
    }

}
