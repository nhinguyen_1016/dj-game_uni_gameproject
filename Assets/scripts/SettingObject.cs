using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[CreateAssetMenu(fileName = "SettingObject", menuName = "Create New SettingObject")]
public class SettingObject : ScriptableObject
{
   public float volume;
   public struct resolution{int x; int y;};

   public float getNormalizedVolume(){
      return 1 + volume/80;
   }
}
