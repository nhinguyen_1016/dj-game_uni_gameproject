using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Deck", menuName = "Create New Deck", order = 0)]
public class Deck : ScriptableObject {
    int maxSize = 5;
    public List<Sample> entities = new List<Sample>();

    public void changeSize(int value)
    {
        if(value >= 0){
            maxSize = value;
        }
        else maxSize = 0;
    }

    public int getMaxSize()
    {
        return maxSize;
    }

    public void addSample(Sample toAdd){
        if(toAdd.unlocked && (entities.Count < maxSize)){
            entities.Add(toAdd);
        }
        // else...
    }

    public void removeSample(Sample toRemove){
        entities.Remove(toRemove);
    }
}
