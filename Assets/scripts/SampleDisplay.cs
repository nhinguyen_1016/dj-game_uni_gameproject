using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SampleDisplay : MonoBehaviour
{

    public Text songName;
    public Text rock;
    public Text funk;
    public Text jazz;
    public Text techno;
    public Text pop;
    public Text hiphop;

    public delegate void SampleDisplayDelegate(Sample s);
    public static event SampleDisplayDelegate onClick;

    public Sample sample;
    AudioSource audioSource;
    bool playing;

    public SettingObject setting;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        playing = false;

        if(sample != null){
            Prime(sample);
            // audioSource.clip = sample.aClip;
        } 

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void Prime(Sample sample)
    {

        this.sample = sample;
        if(nothingIsNull()){
            songName.text = sample.title + "\n" + sample.composer;

            if(sample.rock >= 3 ){
                rock.text = "Rock";
            }
            if(sample.funk >= 3 ){
                funk.text = "Funk";
            }
            if(sample.jazz >= 3 ){
                jazz.text = "Jazz";
            }
            if(sample.techno >= 3 ){
                techno.text = "Techno";
            }
            if(sample.pop >= 3 ){
                pop.text = "Pop";
            }
            if(sample.hiphop >= 3 ){
                hiphop.text = "Hiphop";
            }
        }
    }

    private bool nothingIsNull()
    {
        return (songName != null) && (rock != null) && (funk != null) && (jazz != null) && (techno != null) && (pop != null) && (hiphop != null);
    }

    public void Click()
    {
        string composer = "nothing";
        if(sample.composer != null) 
            composer = sample.composer;

        //Debug.Log("Clicked on: " + composer);
        
        if(onClick != null){
            onClick.Invoke(sample);
        }
        else{
            Debug.Log("Noone listening.");
        }
    }

    public void PlaySample(){
        //check to see whether another sample is already playing
            if(!audioSource.isPlaying){
                // Debug.Log(MainMenuAudioGod.getInstance());
                MainMenuAudioGod.getInstance().muteBGSong();
                audioSource.PlayOneShot(sample.aClip, setting.getNormalizedVolume());
                playing = true;
            }else{
                audioSource.Stop();
            }
    }

    public void PauseSample(){
        audioSource.Pause();
        playing = false;
        MainMenuAudioGod.getInstance().unmuteBGSong();
    }

    public void stopAll(){
        
         foreach(GameObject s in GameObject.FindGameObjectsWithTag("DeckEntityDisplay")){
            s.GetComponent<AudioSource>().Stop();
        }
    }
}
