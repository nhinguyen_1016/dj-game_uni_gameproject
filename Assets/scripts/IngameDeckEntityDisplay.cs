using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngameDeckEntityDisplay : MonoBehaviour
{
    public Text songName;
    public Text timeText;
    private Sample sample;

    string aClipLength;

    // Start is called before the first frame update
    void Start()
    {
        if(sample != null) Prime(sample);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Prime(Sample s){
        if((songName != null))
        {
            if(s != null){
                songName.text = s.composer + " -\n" + s.title;
                sample = s;
                string clipMinutes = ((int) sample.aClip.length / 60).ToString("00");
                string clipSeconds = (sample.aClip.length % 60).ToString("00");
                aClipLength = clipMinutes + ":" + clipSeconds;
            }
            else{
                songName.text = "Empty";
                sample = null;
            }
        }
    }

    public void setTime(float secs){
        if(sample != null){
            string minutes = ((int) secs / 60).ToString("00");
            string seconds = (secs % 60).ToString("00");
            timeText.text = minutes + ":" + seconds + " / " + aClipLength;
        }
        else{
            timeText.text = "No time.";
        }
    }

    public void moveLeft(){
        //Debug.Log("Moving " + sample.title + " to the left.");
        MixConsole.getInstance().setSample(true, sample);
    }
    public void moveRight(){
        //Debug.Log("Moving " + sample.title + " to the right.");
        MixConsole.getInstance().setSample(false, sample);
    }
    public void goBack(bool isL){
        if(sample != null){ 
            //Debug.Log("Moving '" + sample.title + "' back.");
            MixConsole.getInstance().backButtOnClicked(isL, sample);
            sample = null;
        }
    }
}
