using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameAudioGod : MonoBehaviour
{
    public AudioSource aSource;
    public SettingObject setting;
    public AudioClip buttonClickOn, buttonClickOff, sliderMove;
    public AudioClip[] cheer, boo;

    private static IngameAudioGod _instance;
    public static IngameAudioGod getInstance()
    {
        return _instance;
    }

    public void playBClickOn(){
        aSource.PlayOneShot(buttonClickOn, setting.getNormalizedVolume());
    }

    public void playBClickOff(){
        aSource.PlayOneShot(buttonClickOff, setting.getNormalizedVolume());
    }

    public void playSlideMove(){
         aSource.PlayOneShot(sliderMove, setting.getNormalizedVolume());
    }

    public void playCheer(){
        aSource.PlayOneShot(cheer[Random.Range(0, cheer.Length)], setting.getNormalizedVolume());
    }

    public void playBoo(){
        aSource.PlayOneShot(boo[Random.Range(0, cheer.Length)], setting.getNormalizedVolume());
    }

   private void Awake() {
    _instance = this;
   }
}
