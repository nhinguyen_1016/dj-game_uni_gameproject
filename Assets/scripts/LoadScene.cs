using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public void loadScene(int scene){
         SceneManager.LoadScene(scene);
    }

    public void quitGame(){
        Debug.Log("Quit Game!");
        Application.Quit();
    }

}
