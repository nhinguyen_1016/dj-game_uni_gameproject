using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchSlider : MonoBehaviour
{
    private Vector3 startPosition;
    private Vector3 endPosition;
    private MixConsole mixConsole;
    [Range(0, 2)]
    public float pitch;
    private Camera mainCamera;
    private bool start;
    private float currentDistanz;
    private RaycastHit hit;
    [SerializeField]
    private LayerMask mask;
    private Ray currentRay;
    [SerializeField]
    GameObject pitchSlider;

    float doubleClickTime = .2f, lastClickTime;

    public IngameAudioGod IngameAudioGod;

    // Start is called before the first frame update
    void Start()
    {
        mixConsole = MixConsole.getInstance();
        mainCamera = Camera.main;
        startPosition = pitchSlider.transform.position;
        endPosition = startPosition + new Vector3(0.0f, 0.45f, 0.45f);
        pitch = 1;
        pitchSlider.transform.position = Vector3.Lerp(startPosition, endPosition, 0.5f);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            
            currentRay = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(currentRay, out hit, 50.0f, mask))
            {
                if (transform == hit.transform)
                {
                    IngameAudioGod.playSlideMove();
                     float timeSinceLastClick = Time.time - lastClickTime;

                    if (timeSinceLastClick <= doubleClickTime){
                        start = false;
                        pitchSlider.transform.position = Vector3.Lerp(startPosition, endPosition, 0.5f);
                        pitch = 1f;
                        Debug.Log("Double");
                    }else{
                        start = true;
                        calcPitch();
                        currentDistanz = 0;
                    }

                    lastClickTime = Time.time;
                }
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (start)
            {
                start = false;
                currentDistanz = 0;
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (start)
            {
                calcPitch(); 
            }
        }
        UpdatePitch(pitch);
    }

    private void calcPitch()
    {
        currentRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        Vector3 currentPoint = currentRay.GetPoint(hit.distance);
        float distanz = (currentPoint.y - startPosition.y) / Mathf.Abs(startPosition.y -  endPosition.y);
        currentDistanz = Mathf.Max(0, Mathf.Min(distanz, 1));
        currentDistanz = currentDistanz switch
        {
            float f when f < 0.05f => 0,
            float f when f >= 0.05f && f < 0.15f => 0.1f,
            float f when f >= 0.15f && f < 0.25f => 0.2f,
            float f when f >= 0.25f && f < 0.35f => 0.3f,
            float f when f >= 0.35f && f < 0.45f => 0.4f,
            float f when f >= 0.45f && f < 0.55f => 0.5f,
            float f when f >= 0.55f && f < 0.65f => 0.6f,
            float f when f >= 0.65f && f < 0.75f => 0.7f,
            float f when f >= 0.75f && f < 0.85f => 0.8f,
            float f when f >= 0.85f && f < 0.95f => 0.9f,
            float f when f >=  0.95f => 1f,
            _ => 0f
        };
        pitchSlider.transform.position = Vector3.Lerp(startPosition, endPosition, currentDistanz);
        pitch = currentDistanz * 2f;
    }

    private void UpdatePitch(float pitch)
    {
        if(this.name.Equals("PitchRangeLeft")){
            mixConsole.changePitch(pitch, true);
        }else{
            mixConsole.changePitch(pitch, false);
        }
    }

}
