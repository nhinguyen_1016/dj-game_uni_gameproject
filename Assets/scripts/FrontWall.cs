using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontWall : MonoBehaviour
{
    AudioListener listener;
    Renderer rend;
    float[] spectrum = new float[1024];
    float[] lowColor = {0.0f, 0.0f, 0.0f};
    float[] highColor = {0.0f, 0.0f, 0.0f};
    int currentGenre = -1;

    private MixConsole console;
    Sample sampleL;
    Sample sampleR;
    AudioSource audioSourceL;
    AudioSource audioSourceR;


    // Start is called before the first frame update
    void Start()
    {
        listener = GetComponent<AudioListener>();
        rend = GetComponent<Renderer>();
        console = MixConsole.getInstance();
        sampleL = console.getSampleL();
        sampleR = console.getSampleR();

        audioSourceL = console.getAudioSourceL();
        audioSourceR = console.getAudioSourceR();
    }

    // smooth interpolation between start and end based on step value
    float smoothstep(float start, float end, float step){

        // clamp between 0 and 1
        step = (step > 1f) ? 1f : step;
        step = (step < 0f) ? 0f : step;

        step = (step * step ) * ( 3f - ( 2f * step ));

        return start + ( ( end - start ) * step);
    }

    int getTopGenreL(){
        int topGenre = -1;
        int rock = sampleL.rock;
        int funk = sampleL.funk;
        int jazz = sampleL.jazz;
        int techno = sampleL.techno;
        int pop = sampleL.pop;
        int hiphop = sampleL.hiphop;
        int[] genres = {rock, funk, jazz, techno, pop, hiphop};
        int highV = 0;
        for(int i = 0; i < genres.Length; i++){
            if(genres[i] > highV){
                topGenre = i;
                highV = genres[i];
            }
        }
        return topGenre;
    }

    int getTopGenreR(){
        int topGenre = -1;
        int rock = sampleR.rock;
        int funk = sampleR.funk;
        int jazz = sampleR.jazz;
        int techno = sampleR.techno;
        int pop = sampleR.pop;
        int hiphop = sampleR.hiphop;
        int[] genres = {rock, funk, jazz, techno, pop, hiphop};
        int highV = 0;
        for(int i = 0; i < genres.Length; i++){
            if(genres[i] > highV){
                topGenre = i;
                highV = genres[i];
            }
        }
        return topGenre;
    }

    float[] getHighColor(int genre){
        highColor = new float[] {0.0f, 0.0f, 0.0f};

        if(genre == 0){
            highColor = new float[] {1.0f, 0.0f, 0.2f};
        }
        // Funk
        if(genre == 1){
            highColor = new float[] {1.0f, 1.0f, 1.0f};
        }
        // Jazz
        if(genre == 2){
            highColor = new float[] {0.7f, 0.6f, 0.0f};
        }
        // Techno 
        if(genre == 3){
            highColor = new float[] {0.7f, 0.0f, 0.9f};
        }
        // Pop
        if(genre == 4){
            highColor = new float[] {1.0f, 0.7f, 0.8f};
        }
        // Hip-Hip
        if(genre == 5){
            highColor = new float[] {0.1f, 0.2f, 0.9f};
        }
        return highColor;
    }
    float[] getLowColor(int genre){
        lowColor = new float[] {0.0f, 0.0f, 0.0f};
        if(genre == 0){
            lowColor = new float[] {0.2f, 0.0f, 0.1f};
        }
        // Funk
        if(genre == 1){
            lowColor = new float[] {0.0f, 0.0f, 0.0f};
        }
        // Jazz
        if(genre == 2){
            lowColor = new float[] {0.2f, 0.1f, 0.0f};
        }
        // Techno 
        if(genre == 3){
            lowColor = new float[] {0.4f, 0.0f, 0.2f};
        }
        // Pop
        if(genre == 4){
            lowColor = new float[] {0.1f, 0.2f, 0.2f};
        }
        // Hip-Hip
        if(genre == 5){
            lowColor = new float[] {0.1f, 0.3f, 0.3f};
        }
        return lowColor;
    }


    // Update is called once per frame
    void Update()
    {
        sampleL = console.getSampleL();
        sampleR = console.getSampleR();

        // check if both samples are set, if not then no interpolation necessary
        if(sampleL && sampleR){
            int genreR = getTopGenreR();
            int genreL = getTopGenreL();

            float transitionAmountLeft = console.getTransitionLeft();
            float[] highColorR = getHighColor(genreR);
            float[] lowColorR = getLowColor(genreR);

            float[] highColorL = getHighColor(genreL);
            float[] lowColorL = getLowColor(genreL);

            // interpolated high color
            float rRightH = highColorR[0];
            float gRightH = highColorR[1];
            float bRightH = highColorR[2];

            float rLeftH = highColorL[0];
            float gLeftH = highColorL[1];
            float bLeftH = highColorL[2];

            float rHigh = smoothstep(rRightH, rLeftH, transitionAmountLeft);
            float gHigh = smoothstep(gRightH, gLeftH, transitionAmountLeft);
            float bHigh = smoothstep(bRightH, bLeftH, transitionAmountLeft);

            highColor = new float[] {rHigh, gHigh, bHigh};

            // interpolated low color
            float rRightL = lowColorR[0];
            float gRightL = lowColorR[1];
            float bRightL = lowColorR[2];

            float rLeftL = lowColorL[0];
            float gLeftL = lowColorL[1];
            float bLeftL = lowColorL[2];

            float rLow = smoothstep(rRightL, rLeftL, transitionAmountLeft);
            float gLow = smoothstep(gRightL, gLeftL, transitionAmountLeft);
            float bLow = smoothstep(bRightL, bLeftL, transitionAmountLeft);

            lowColor = new float[] {rLow, gLow, bLow};

        }else if(sampleL){
            int genre = getTopGenreL();
            highColor = getHighColor(genre);
            lowColor = getLowColor(genre);
        }else if(sampleR){
            int genre = getTopGenreR();
            highColor = getHighColor(genre);
            lowColor = getLowColor(genre);
        }

        AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);
        float maxData = 0.0f;
        for(int i = 0; i < 500; i++){
            float val = spectrum[i];
            if( val > maxData){
                maxData = val;
            }
        }
        rend.material.SetFloat("_val", maxData);
        rend.material.SetFloatArray("lowColor", lowColor);
        rend.material.SetFloatArray("highColor", highColor);
    }
}