using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDeckCanvas : MonoBehaviour
{
    public static void hideDeckCanvas(){            
        foreach (GameObject scroll in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            if(scroll.name.Equals("SizeText") || scroll.name.Equals("SampleListText") || scroll.name.Equals("DeckText") || scroll.name.Equals("PanelScroll") || scroll.name.Equals("PanelScroll 2")){
                
                if(scroll.activeInHierarchy){
                    Debug.Log("Scroll is active. Set to non-active.");
                    scroll.SetActive(false);
                }else{
                    Debug.Log("Scroll is non-active. Set to active.");
                    scroll.SetActive(true);
                }
            }
                
        }
    }
    
}
