using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuAudioGod : MonoBehaviour
{
    public AudioSource aSource;
    public AudioClip hover;
    public AudioClip click;
    public AudioClip buttonClick;
    public SettingObject setting;

    // Speziell für das Lied, besser als ein ganzes Objekt samt Skript zu erstellen.
    public AudioSource songSource;
    
    private static MainMenuAudioGod _instance;
    public static MainMenuAudioGod getInstance()
    {
        return _instance;
    }

    void Start(){
        songSource.volume = setting.getNormalizedVolume() * 0.2f;
        aSource.volume = setting.getNormalizedVolume();
    }

    void Awake()
    {
        _instance = this;
    }
    
    void Update(){
        if(songSource.volume != (setting.getNormalizedVolume() * 0.2f)){
            songSource.volume = setting.getNormalizedVolume() * 0.2f;
        }
        if(aSource.volume != setting.getNormalizedVolume()){
            aSource.volume = setting.getNormalizedVolume();
        }   
    }

    public void playClickSound(){
        aSource.PlayOneShot(click, setting.getNormalizedVolume());
    }
    
    public void playHoverSound(){
        aSource.PlayOneShot(hover, setting.getNormalizedVolume());
    }

    public void playButtonClickSound(){
        aSource.PlayOneShot(buttonClick, setting.getNormalizedVolume());
    }
    
    //mute lied
    public void muteBGSong(){
        songSource.mute = true;
    }

    public void unmuteBGSong(){
        songSource.mute = false;
    }

}
