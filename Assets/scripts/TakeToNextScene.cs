using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeToNextScene : MonoBehaviour
{
    [SerializeField]
    private GameObject[] objects;
    // Start is called before the first frame update
    private void Awake() {
        foreach (var item in objects)
        {
            DontDestroyOnLoad(item);    
        }  
    }

}