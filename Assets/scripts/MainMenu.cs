using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Deck deck;

    void Start()
    {

        foreach (GameObject scroll in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            if(scroll.name.Equals("SizeText") || scroll.name.Equals("SampleListText") || scroll.name.Equals("DeckText") || scroll.name.Equals("PanelScroll") || scroll.name.Equals("PanelScroll 2") ){
                scroll.SetActive(false);
            }
        }
       
    }

   public void PlayGame(){
        // loads next level in queue
        // wenn Deckgröße = 0, dann erst Deck erstellen
        if(deck.entities.Count <= 0){
            Debug.Log("Deck ist leer! Erstelle Deck.");
            GameObject mainMenu;
            GameObject levelPrep;
            foreach(GameObject g in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[]){
                if(g.name.Equals("MainMenu")){
                    g.SetActive(false);
                }
                if(g.name.Equals("LevelPrepMenu")){
                    g.SetActive(true);
                }
            }
            EnableDeckCanvas.hideDeckCanvas();
        }
        else{
            Debug.Log("Started a new Game!"); 
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
   }

   public void QuitGame(){
       Debug.Log("Quit Game!"); 
       Application.Quit();
   }
   
}
