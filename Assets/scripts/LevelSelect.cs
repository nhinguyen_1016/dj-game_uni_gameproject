using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.EventSystems;

public class LevelSelect : MonoBehaviour
{
    static int levelNumber;
    [SerializeField]
    private Deck deck;

    public void CheckLevel(){
        string levelName = EventSystem.current.currentSelectedGameObject.name;

        switch(levelName){
            case "Level1Button":
                Debug.Log("Preparing for Level 1.");
                levelNumber = 1;
                break;

            case "Level2Button":
                Debug.Log("Preparing for Level 2.");
                levelNumber = 2;
                break;

        }
       
    }

    public void StartLevel(){
        if(deck.entities.Count > 0){
            Debug.Log("Started Level: " + levelNumber);
            if(levelNumber <= 0){
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
            else{
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + levelNumber);
            }
        }
        else{
            Debug.Log("Deck darf nicht leer sein!");
        }
    }
   
}
