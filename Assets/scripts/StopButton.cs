using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopButton : MonoBehaviour
{

    private Camera mainCamera;
    private Ray ray;
    private RaycastHit hit;
    [SerializeField]
    private LayerMask mask;
    [SerializeField]
    private GameObject pause, play;
    private MixConsole mixConsole;

    public IngameAudioGod IngameAudioGod;

    public void changButtons(){
        play.SetActive(true);
        pause.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        mixConsole = MixConsole.getInstance();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        { 
            ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f, mask))
            {
                if (transform == hit.transform)
                {
                    IngameAudioGod.playBClickOn();
                    changButtons();
                    if (this.name.Equals("StopLeft"))
                    {
                        mixConsole.stopLeft();
                    }
                    else
                    {
                        mixConsole.stopRight();
                    }
                }
            }
        }
    }
}
