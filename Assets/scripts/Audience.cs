using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class Audience : MonoBehaviour
{
    private float timeToChange;
    private const float INTAVAL = 5f;
    private float timeToChangeLight = 0.5f;
    [SerializeField]
    private float time;
    private int[] musicPreferences;
    [SerializeField]
    private Genres[] genres;
    [SerializeField]
    private GameObject endScreen;
    private MixConsole mixConsole;
    [SerializeField]
    private GameObject light;
    private Transform[] lights;
    private bool playing;
    public int score;
    [SerializeField]
    private TextMeshProUGUI endScoreTextField, ingameScoreTextField, timeTextField;
    [SerializeField]
    private GameObject speachRock1, speachRock2, speachFunk1, speachFunk2, speachJazz1, speachJazz2, speachTechno1, speachTechno2, speachPop1, speachPop2, speachHipHop1, speachHipHop2, speachLouder1;
    private float scoreMultiplier = 1.0f;
    [SerializeField]
    private List<GameObject> happyCrowd = new List<GameObject>();
    [SerializeField]
    private List<GameObject> annoyedCrowd = new List<GameObject>();
    private int annoyed = 0;
    private List<GameObject> activeCrowd = new List<GameObject>();
    private List<GameObject> wasActive = new List<GameObject>();
    private int addToScore;
    private float addToScoreTimeout = 1.5f;
    public IngameAudioGod ingameAudioGod;

    private static Audience _instance;

    public static Audience getInstance()
    {
        return _instance;
    }
    
    public void setPlaying(bool playing){
        this.playing = playing;
    }
    public float intgetTime(){
        return time;
    }

    private void calcPreferences(){
        for (int index = 0; index <6; index++)
        {
            foreach (var g in genres)
            {
                if ((Genres)index == g)
                {
                    this.musicPreferences[index] = Random.Range(6, 11);//(minimumInclusive, maximumExclusive)
                    break;
                }else
                {
                    this.musicPreferences[index] = Random.Range(0, 5);
                }
            }
        }
    }

    private void disableSpeech(){
        speachRock1.SetActive(false);
        speachRock2.SetActive(false);
        speachFunk1.SetActive(false);
        speachFunk2.SetActive(false);
        speachJazz1.SetActive(false);
        speachJazz2.SetActive(false);
        speachTechno1.SetActive(false);
        speachTechno2.SetActive(false);
        speachPop1.SetActive(false);
        speachPop2.SetActive(false);
        speachHipHop1.SetActive(false);
        speachHipHop2.SetActive(false);
        speachLouder1.SetActive(false);
    }

    public void pauseSpeech(){
        if(speachRock1.activeSelf){
            this.wasActive.Add(speachRock1);
            speachRock1.SetActive(false);
        }
        if(speachRock2.activeSelf){
            this.wasActive.Add(speachRock2);
            speachRock2.SetActive(false);
        }
        if(speachFunk1.activeSelf){
            this.wasActive.Add(speachFunk1);
            speachFunk1.SetActive(false);
        }
        if(speachFunk2.activeSelf){
            this.wasActive.Add(speachFunk2);
            speachFunk2.SetActive(false);
        }
        if(speachJazz1.activeSelf){
            this.wasActive.Add(speachJazz1);
            speachJazz1.SetActive(false);
        }
        if(speachJazz2.activeSelf){
            this.wasActive.Add(speachJazz2);
            speachJazz2.SetActive(false);
        }
        if(speachTechno1.activeSelf){
            this.wasActive.Add(speachTechno1);
            speachTechno1.SetActive(false);
        }
        if(speachTechno2.activeSelf){
            this.wasActive.Add(speachTechno2);
             speachTechno2.SetActive(false);
        }
        if(speachPop1.activeSelf){
            this.wasActive.Add(speachPop1);
            speachPop1.SetActive(false);
        }
        if(speachPop2.activeSelf){
            this.wasActive.Add(speachPop2);
            speachPop2.SetActive(false);
        }
        if(speachHipHop1.activeSelf){
            this.wasActive.Add(speachHipHop1);
            speachHipHop1.SetActive(false);
        }
         if(speachHipHop2.activeSelf){
            this.wasActive.Add(speachHipHop2);
             speachHipHop2.SetActive(false);
        }
         if(speachLouder1.activeSelf){
            this.wasActive.Add(speachLouder1);
            speachLouder1.SetActive(false);
        }
    }
    public void resumeSpeech(){
        foreach ( GameObject o in wasActive)
        {
            o.SetActive(true);
        }
        wasActive.Clear();
    }
    
    private void shoutout(){
        int inn = 0;
        int dominant = 0;
        foreach (int value in musicPreferences)
        {
            if(musicPreferences[dominant]<value){
                dominant = inn;
            }
            inn++;
        }
        disableSpeech();
        switch ((Genres)dominant)
        {
            case Genres.ROCK:
                if(Random.Range(1,3) == 1) speachRock1.SetActive(true);
                else speachRock2.SetActive(true);
                break;
            case Genres.FUNK:
                if(Random.Range(1,3) == 1) speachFunk1.SetActive(true);
                else  speachFunk2.SetActive(true);
                break;
            case Genres.JAZZ:
                if(Random.Range(1,3) == 1) speachJazz1.SetActive(true);
                else  speachJazz2.SetActive(true);
                break;
            case Genres.TECHNO:
                if(Random.Range(1,3) == 1) speachTechno1.SetActive(true);
                else  speachTechno2.SetActive(true);
                break;
            case Genres.POP:
                if(Random.Range(1,3) == 1) speachPop1.SetActive(true);
                else  speachPop2.SetActive(true);
                break;
            case Genres.HIPHOP:
                if(Random.Range(1,3) == 1) speachHipHop1.SetActive(true);
                else  speachHipHop2.SetActive(true);
                break;
            default:
                Debug.Log((Genres)dominant);
                break;
        }
    }

    private int getScoreRight(){
        int addToScore = 0;
        Sample sample = mixConsole.getSampleR();
        int[] samplGenre = new int[]{
            sample.rock,
            sample.funk,
            sample.jazz,
            sample.techno,
            sample.pop,
            sample.hiphop
            };
        for (int i = 0; i < musicPreferences.Length; ++i)
        {
            addToScore += 10 - Mathf.Abs(samplGenre[i] - musicPreferences[i]);
        }
        return addToScore;
    }
    private int getScoreLeft(){
        int addToScore = 0;
        Sample sample = mixConsole.getSampleL();
        int[] samplGenre = new int[]{
            sample.rock,
            sample.funk,
            sample.jazz,
            sample.techno,
            sample.pop,
            sample.hiphop
            };
        for (int i = 0; i < musicPreferences.Length; ++i)
        {
            addToScore += 10 - Mathf.Abs(samplGenre[i] - musicPreferences[i]);
        }
        return addToScore;
    }
    private int calcScore(){
        AudioSource right = mixConsole.getAudioSourceR();
        AudioSource left = mixConsole.getAudioSourceL();
        int addToScore = 0;
        if(right.isPlaying && left.isPlaying){
            if(right.volume > 0.1 && left.volume > 0.1){
                addToScore = (int)(getScoreLeft() * mixConsole.getTransitionLeft() + getScoreRight() * (1 - mixConsole.getTransitionLeft()));

            }else if(right.volume > 0.1){
                addToScore = getScoreRight();
            
            }else if(left.volume > 0.1){
                addToScore = getScoreLeft();
            }
        }else if(!right.isPlaying && !left.isPlaying || right.volume <= 0.1 && left.volume <= 0.1){
            //shoutout "louder"
            speachLouder1.SetActive(true);
        }else if(right.isPlaying && right.volume > 0.1){
            addToScore = getScoreRight();
            
        }else if(left.isPlaying && left.volume > 0.1){
            addToScore = getScoreLeft();
        }
        setCrowedMood(happyCrowd.Count - Mathf.CeilToInt(addToScore/60f * happyCrowd.Count));
       
        return addToScore;
    }

    void setCrowedMood(int annoyedCount){
        int differenz = annoyed - annoyedCount;
        if (differenz < 0){
            List<GameObject> annoyedNotActive = annoyedCrowd.Except(activeCrowd).ToList();
            List<GameObject> happyAndActive = happyCrowd.Where(i => activeCrowd.Contains(i)).ToList();
            for(int i=0; i>differenz && annoyedNotActive.Count > 0 && happyAndActive.Count > 0 ; i--){
                int index = Random.Range(0, annoyedNotActive.Count);
                activeCrowd.Add(annoyedNotActive[index]);
                annoyedNotActive[index].SetActive(true);
                activeCrowd.Remove(happyAndActive[index]);
                happyAndActive[index].SetActive(false);
                annoyedNotActive.RemoveAt(index);
                happyAndActive.RemoveAt(index);

            }
            annoyed = annoyedCount;
        } else if(differenz > 0){
            List<GameObject> annoyedAndActive = annoyedCrowd.Where(i => activeCrowd.Contains(i)).ToList();
            List<GameObject> happyNotActive = happyCrowd.Except(activeCrowd).ToList();
            for(int i=0; i<differenz && annoyedAndActive.Count > 0 && happyNotActive.Count > 0 ; i++){
                int index =   Random.Range(0, happyNotActive.Count);
                activeCrowd.Add(happyNotActive[index]);
                happyNotActive[index].SetActive(true);
                activeCrowd.Remove(annoyedAndActive[index]);
                annoyedAndActive[index].SetActive(false);
                annoyedAndActive.RemoveAt(index);
                happyNotActive.RemoveAt(index);
            }
            annoyed = annoyedCount;
        }
    }

    public void onGoodTransition(){
        scoreMultiplier += 0.3f; 
    }

    public void onBadTransition(){
        ingameAudioGod.playBoo();
        scoreMultiplier = 1.0f;
    }

    private void changeLights(){
        int r = Random.Range(0,5); 
        foreach (Transform l in lights)
        {
            if (r % 4 == 0) {
                l.gameObject.SetActive(true);
            }else {
                l.gameObject.SetActive(false); //deactivates light(parent) as well sometimes
            }
            r++;
        }
        light.SetActive(true);   //light needs to be active to have  activeInHierarchy == true for all children      

        timeToChangeLight = 0.5f;
    }

    void Awake()
    {
        _instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {    
        this.timeToChange = INTAVAL;
        playing = true;
        score = 0;
        mixConsole = MixConsole.getInstance();
        ingameAudioGod = IngameAudioGod.getInstance();
        this.musicPreferences = new int[6];
        calcPreferences();
        shoutout();
        lights = light.GetComponentsInChildren<Transform>(true);
        changeLights();
        foreach(GameObject c in happyCrowd){
            if (c.activeSelf)  activeCrowd.Add(c);
        }
        foreach(GameObject c in annoyedCrowd){
            if (c.activeSelf)  activeCrowd.Add(c);
        }    
    }

    // Update is called once per frame
    void Update()
    {
        if(playing){
            this.addToScore += Mathf.CeilToInt(calcScore() * Time.deltaTime * this.scoreMultiplier);
            if(time<=0){
                //stop music
                this.mixConsole.stopLeft();
                this.mixConsole.stopRight();
                playing = false;
                score += this.addToScore;
                this.addToScore = 0;
                //enable endscreen
                ingameScoreTextField.SetText("Score: \n {0}\n x{1}", score, this.scoreMultiplier);
                endScoreTextField.SetText("Score: \n {0}", score);
                endScreen.SetActive(true);
                // mixConsole.resetSample();
                disableSpeech();
            }else if(this.timeToChange<=0){
                this.timeToChange = INTAVAL;
                score += this.addToScore;
                ingameScoreTextField.SetText("Score: \n {0} + {1}\n x{2}", score, this.addToScore, this.scoreMultiplier);
                addToScoreTimeout = 1.5f;
                if(addToScore < 50) ingameAudioGod.playBoo();
                else if((addToScore / scoreMultiplier) >= 300) ingameAudioGod.playCheer();
                this.addToScore = 0;
                calcPreferences();
                shoutout();
            }
            if(this.addToScoreTimeout<=0 && this.addToScoreTimeout>=-0.01){ 
                ingameScoreTextField.SetText("Score: \n {0}\n x{1}", score, this.scoreMultiplier);
            }else{
                this.addToScoreTimeout  -= Time.deltaTime;
            }
            if(timeToChangeLight <= 0){
               changeLights();
            }
            timeToChangeLight -= Time.deltaTime;
            this.timeToChange -= Time.deltaTime;
            this.time -= Time.deltaTime;
            this.timeTextField.SetText("{0}:{1:00}", (int)time/60, (int)time%60);
        } 
    }
}
