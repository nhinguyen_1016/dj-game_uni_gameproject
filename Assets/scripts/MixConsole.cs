using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MixConsole : MonoBehaviour
{
    public Deck deck;
    public Audience audience;
    public IngameDeckDisplay canvasPrefab;
    public static bool veraendert = false;
    IngameDeckDisplay canvas;
    float sampleRTime;
    float sampleLTime;

    Sample sampleL = null;
    Sample sampleR = null;
    public SettingObject setting;
    AudioSource audioSourceL;
    AudioSource audioSourceR;
    [SerializeField]
    private StopButton stopButtonLeft, stopButtonRight;

    private float transitionAmountLeft = 1f;
    private float speedL = 1f;
    private float speedR = 1f;
    private float pitchL = 1f;
    private float pitchR = 1f;

    private float[] spectrumL = new float[1024];
    private float[] spectrumR = new float[1024];


    private static MixConsole _instance;

// getter
    public static MixConsole getInstance()
    {
        return _instance;
    }

    public Sample getSampleL()
    {
        return sampleL;
    }
    public Sample getSampleR()
    {
        return sampleR;
    }

    public AudioSource getAudioSourceL()
    {
        return audioSourceL;
    }
    public AudioSource getAudioSourceR()
    {
        return audioSourceR;
    }
    public float getTransitionLeft(){
        return transitionAmountLeft;
    }

    public void changePitch(float changeBy, bool isLeft)
    {
        if (isLeft)
        {
            pitchL = changeBy;
        }
        else
        {
            pitchR = changeBy;
        }
        calculatePitch(isLeft);
    }

    private void calculatePitch(bool isLeft)
    {
        if (isLeft)
        {
            float pitch = Mathf.Min(pitchL * speedL, 2f);
            audioSourceL.pitch = pitch;
        }
        else
        {
            float pitch = Mathf.Min(pitchR * speedR, 2f);
            audioSourceR.pitch = pitch;
        }
    }

    public void changeVolume(float volume, bool isLeft)
    {
        if (isLeft)
        {
            audioSourceL.volume = volume * transitionAmountLeft * setting.getNormalizedVolume();

            // check for first sample play of the game
            if( audioSourceL.volume > 0.0 && sampleL && !sampleR){
                sampleL.played = true;
            }

            if(
                sampleL && 
                sampleR && 
                !sampleL.played && 
                audioSourceL.isPlaying &&
                audioSourceR.isPlaying && 
                audioSourceL.volume > 0.0 && 
                audioSourceR.volume > 0.0)
            {
                sampleL.played = true;
                onTransition();
            }
        }
        else
        {
            float transitionAmountRight = 1 - transitionAmountLeft;
            audioSourceR.volume = volume * transitionAmountRight* setting.getNormalizedVolume();
            if( audioSourceR.volume > 0.0 && sampleR && !sampleL){
                sampleR.played = true;
            }

            if(
                sampleL && 
                sampleR && 
                !sampleR.played && 
                audioSourceL.isPlaying &&
                audioSourceR.isPlaying && 
                audioSourceL.volume > 0.0 && 
                audioSourceR.volume > 0.0)
            {
                sampleR.played = true;
                onTransition();
            }
        }
    }

    public void changeSpeed(float changeBy, bool isLeft)
    {
        if (isLeft)
        {
            speedL = changeBy;
            calculatePitch(isLeft);
        }
        else
        {
            speedR = changeBy;
            calculatePitch(isLeft);
        }
    }

    public void mixSamples(float valueL, float valueR)
    {
        transitionAmountLeft = valueL;
    }

    public void setSample(bool isL, Sample sample)
    {
        if (isL)
        {
            setSampleL(sample);
        }
        else
        {
            setSampleR(sample);
        }
        veraendert = true;
    }

    public void backButtOnClicked(bool isL, Sample s){
        if(isL){
            sampleL = null;
            audioSourceL.clip = null;
            stopButtonLeft.changButtons();
            // audioSourceL.Stop();
        }
        else{
            sampleR = null;
            audioSourceR.clip = null;
            stopButtonRight.changButtons();
            // audioSourceR.Stop();
        }
        veraendert = true;
    }
    
    private void setSampleL(Sample sample)
    {
        stopButtonLeft.changButtons();
        sampleL = sample;
        audioSourceL.clip = sampleL.aClip;

        sample.played = false;
    }

    private void setSampleR(Sample sample)
    {
        stopButtonRight.changButtons();
        sampleR = sample;
        audioSourceR.clip = sampleR.aClip;

        sample.played = false;
    }

    public void onTransition(){
        float highestAmplitudeSampleL = 0.0f;
        float highestAmplitudeSampleR = 0.0f;

        audioSourceL.GetSpectrumData(spectrumL, 0, FFTWindow.Rectangular);
        audioSourceR.GetSpectrumData(spectrumR, 0, FFTWindow.Rectangular);

        int loudestFreqL = -1;
        int loudestFreqR = -1;

        for(int i = 0; i < spectrumL.Length; i++){
            float ampL = spectrumL[i];
            float ampR = spectrumR[i];

            if(ampL > highestAmplitudeSampleL){
                highestAmplitudeSampleL = ampL;
                loudestFreqL = i;
            }
            if(ampR > highestAmplitudeSampleR){
                highestAmplitudeSampleR = ampR;
                loudestFreqR = i;
            }
        }
        if( ( Mathf.Abs( highestAmplitudeSampleL - highestAmplitudeSampleR ) > 0.05 ) && ( Mathf.Abs(loudestFreqL - loudestFreqR)  > 800 ) ){
            onBadTransition();
        }else{
            onGoodTransition();
        }
    }

    public void onGoodTransition(){
        audience.onGoodTransition();
    }

    public void onBadTransition(){
        audience.onBadTransition();
    }

    public bool isOnDisplay(Sample s){
        return (s == sampleL) || (s == sampleR);
    }

    public void playLeft()
    {
        audioSourceL.Play();
        sampleLTime = audioSourceL.time;  
    }
    public void playRight()
    {
        audioSourceR.Play();
        sampleRTime = audioSourceR.time;  
    }
    public void pauseLeft(){
        audioSourceL.Pause();
    }
    public void pauseRight(){
        audioSourceR.Pause();
    }
    public void stopLeft(){
        audioSourceL.Stop();
        //sende 0 sekunden an sampleDisplayLeft
        sampleLTime = 0.0f;
        canvas.setTimes(true, sampleLTime);
    }
    public void stopRight(){
        audioSourceR.Stop();
        //sende 0 sekunden an sampleDisplayRight
        sampleRTime = 0.0f;
        canvas.setTimes(false, sampleRTime);
    }
    void Awake()
    {
        _instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        AudioSource[] audioSource = GetComponents<AudioSource>();
        audioSourceL = audioSource[0];
        audioSourceR = audioSource[1];
        // Canvas erörtern
        canvas = (IngameDeckDisplay)Instantiate(canvasPrefab);
        canvas.Prime(deck.entities);
        canvas.PrimeScreens(sampleL, sampleR);
        // Zeit geben.
        canvas.setTimes(true, sampleLTime);
        canvas.setTimes(false, sampleRTime);

        audience = Audience.getInstance();
    }

    // Update is called once per frame
    void Update()
    {
        // Canvas checken
        if(veraendert){
            canvas.Prime(deck.entities);
            canvas.PrimeScreens(sampleL, sampleR);
            sampleLTime = 0.0f;
            canvas.setTimes(true, sampleLTime);
            sampleRTime = 0.0f;
            canvas.setTimes(false, sampleRTime);
            veraendert = false;
        }
        // Wenn ASources spielen, dessen Zeit übermitteln.
        if(audioSourceL.isPlaying && (audioSourceL.time != sampleLTime)){
            sampleLTime = audioSourceL.time;
            canvas.setTimes(true, sampleLTime);
        }
        if(audioSourceR.isPlaying && (audioSourceR.time != sampleRTime)){
            sampleRTime = audioSourceR.time;
            canvas.setTimes(false, sampleRTime);
        }
    }

}
