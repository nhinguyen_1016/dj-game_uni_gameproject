using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SampleListDisplay : MonoBehaviour
{
    public Transform sampleTransform;
    public SampleDisplay sampleDisplayPrefab;

    public Transform deckTransform;
    public SampleDisplay deckDisplayPrefab;

    public TMP_Text sizeText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Prime(List<Sample> samples)
    {
        // Loesche alle vorher erzeugten SampleDisplays
        for(int i = 0; i < sampleTransform.childCount; i++){
            GameObject child = sampleTransform.GetChild(i).gameObject;
            Destroy(child);
        }
        // fuege übergebene Liste hinzu
        // Soundliste, alle freigeschalteten Listen
        foreach(Sample s in samples){
            SampleDisplay sDisplay = (SampleDisplay)Instantiate(sampleDisplayPrefab);
            sDisplay.transform.SetParent(sampleTransform, false);
            sDisplay.Prime(s);
        }
    }

    public void PrimeDeck(List<Sample> samples)
    {
        // Loesche alle vorher erzeugten SampleDisplays
        for(int i = 0; i < deckTransform.childCount; i++){
            GameObject child = deckTransform.GetChild(i).gameObject;
            Destroy(child);
        }
        // fuege übergebene Liste hinzu
        // Deck, alle Samples in Deck's entities Liste
        foreach(Sample s in samples){
            SampleDisplay sDisplay = (SampleDisplay)Instantiate(deckDisplayPrefab);
            sDisplay.transform.SetParent(deckTransform, false);
            sDisplay.Prime(s);
        }
    }

    public void PrimeText(int lSize, int maxSize){
        sizeText.text = lSize + " / " + maxSize;

    }
}
