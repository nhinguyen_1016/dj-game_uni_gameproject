using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleResponder : MonoBehaviour
{
    public Deck deck;
    // Start is called before the first frame update
    void Start()
    {
        SampleDisplay.onClick += HandleonClick;
    }

    private void OnDestroy() {
        //Debug.Log("unsigned-up for onClick");
        SampleDisplay.onClick -= HandleonClick;
    }

    void HandleonClick(Sample sample)
    {
        //Debug.Log("God is listening. Sing to god. Sing to " + sample.title);
        // vom Deck aus oder von Liste?
        if(deck.entities.IndexOf(sample) >= 0){
            //Debug.Log("DeckEntitaet.");
            deck.removeSample(sample);
            SampleListe.neueAenderung = true;
        }
        else{
            //Debug.Log("Soundliste.");
            deck.addSample(sample);
            SampleListe.neueAenderung = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
