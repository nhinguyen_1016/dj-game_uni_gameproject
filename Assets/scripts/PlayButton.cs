using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : MonoBehaviour
{
    private Camera mainCamera;
    private Ray ray;
    private RaycastHit hit;
    [SerializeField]
    private LayerMask mask;
    [SerializeField]
    private GameObject pause, play;
    private MixConsole mixConsole;

    public IngameAudioGod IngameAudioGod;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        mixConsole = MixConsole.getInstance();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        { 
            ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f, mask))
            {
                if (transform == hit.transform)
                {
                    IngameAudioGod.playBClickOn();
                    play.SetActive(false);
                    pause.SetActive(true);
                    if (this.name.Equals("PlayLeft"))
                    {
                        mixConsole.playLeft();
                    }
                    else
                    {
                        mixConsole.playRight();
                    }
                }
            }


        }
    }
    //equal to raycast in update
    // private void OnMouseDown(){
    //     Debug.Log(renderer.material.mainTexture.name);
    //     renderer.material.mainTexture  = 
    //         renderer.material.mainTexture  == play ? pause : play;
    // }
}
