// // Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// // Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader "Custom/shaderTest"
// {
//     Properties
//     {
//         _MainTex ("Texture", 2D) = "white" {}
//         // audiosource
//         _ColorTint("ColorTint", Color) = (1, 0, 0, 0)
//         _b("b", Range (0, 1)) = 0
//     }
//     SubShader
//     {
//         Tags { "RenderType"="Opaque" }
//         LOD 100

//         Pass
//         {
//             CGPROGRAM
//             #pragma vertex vert
//             #pragma fragment frag
//             #include "UnityCG.cginc"

//             struct v2f
//             {
//                 float4 position : SV_POSITION;
//             };

//             v2f vert(float4 v:POSITION) : SV_POSITION {
//                 v2f o;
//                 o.position = UnityObjectToClipPos (v);
//                 return o;
//             }

//             sampler2D _MainTex;
//             float4 _MainTex_ST;
//             float4 _ColorTint;
//             int _b;

//             float hash21(float p) {
//                 return frac(sin(dot(p, float2(12.9898, 78.233))) * 43758.5453);
//             }

//             fixed4 frag (v2f i) : SV_Target
//             {   
//                 float t = _Time.y;
//                 float2 uv =  -1 + 2. * i.position.xy / _ScreenParams.xy;
//                 fixed4 col = fixed4(.3, .5, .1, 1.);
//                 float wave = sin(-uv.x + (t * 1.5) * 3.14) + sin(-uv.y + t * 3.14);

//                 col.x = mul(col.x, wave);
//                 col.y = mul(col.y, wave);
//                 return col;
//             }

//             ENDCG
//         }
//     }
// }
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/shaderTest"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        // audiosource
        _ColorTint("ColorTint", Color) = (1, 0, 0, 0)
        _b("b", Range (0, 1)) = 0
        
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION; // vertex position
                float2 uv : TEXCOORD0; // texture coordinate
            };


            struct v2f
            {
                float2 uv : TEXCOORD0; // texture coordinate
                float4 vertex : SV_POSITION; // clip space position
            };

            v2f vert(appdata v) {
                v2f o;
                // transform position to clip space
                // (multiply with model*view*projection matrix)
                o.vertex = UnityObjectToClipPos(v.vertex);
                // just pass the texture coordinate
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _ColorTint;
            int _b;
            float _freq[1020];
            float _val;
            float lowColor[3];
            float highColor[3];

            float hash21(float p) {
                return frac(sin(dot(p, float2(12.9898, 78.233))) * 43758.5453);
            }

            float random(float2 st){
                return frac(sin(dot(st, float2(12.9898,78.233))) * 43758.5453123);
            }
            float noise(float2 st){
                // integer and float component
                float2 i = floor(st);
                float2 f = frac(st);

                // 4 corners of a square
                float a = random(i);
                float b = random(i + float2(1. , 0.));
                float c = random(i + float2(0. , 1.));
                float d = random(i + float2(1. , 1.));

                float2 u = smoothstep(0.0, 1.0, f );

                return lerp(a, b, u.x) + (c - a) * u.y * (1. - u.x) + (d - b) * u.x * u.y;
            }


            fixed3 raymarcher(fixed3 rayPos, fixed3 rayDir){
                float dist = 0.;
                float maxDist = 200.;
                int steps = 0;
                int maxSteps = 200;

                fixed3 color = fixed3(0.0, 0.0, 0.0);
                fixed3 highCol = fixed3( highColor[0], highColor[1], highColor[2]);
                fixed3 lowCol = fixed3( lowColor[0], lowColor[1], lowColor[2]);

                for(steps; steps < maxSteps; ++steps){
                    fixed3 pos = rayPos + dist * rayDir;

                    float beat = _val;

                    float height = (noise(pos.xz)  * (  (dist) ));
                    height = height * beat;

                    if(dist > maxDist){
                      break;
                    }
                    if(height >= pos.y){
                      color = lerp(lowCol, highCol, height * 0.1);
                      break;
                    }
                    dist += .7;
                }

                float ambientOcclusion = float(steps)/float(maxSteps);

                color = color * fixed3(1. - ambientOcclusion, 1. - ambientOcclusion, 1. - ambientOcclusion);
                return color;
            }


            fixed4 frag (v2f i) : SV_Target
            {   
                fixed2 uv = i.uv;
                uv = uv - 0.5;
                uv = uv * 10.;

                float u_time = _Time[2];

                fixed4 col = fixed4(0.0, 0.0, 0.0, 1.);
                
                fixed3 camera = fixed3(0., 5., 0.);
                fixed3 lookAt = fixed3(0., 10., 200.);

                camera.z += u_time * 3.;
                lookAt.z += u_time * 3.;


                fixed3 z = normalize(lookAt - camera);
                fixed3 x = normalize(cross(z, fixed3(0., 1., 0.)));
                fixed3 y = normalize(cross(x, z));
                fixed3 rayDir = normalize(fixed3( z + uv.x * x + uv.y * y ));
                fixed3 color = raymarcher(camera, rayDir);
                col = fixed4(color, 1.0);

                return col;
            }

            ENDCG
        }
    }
}

